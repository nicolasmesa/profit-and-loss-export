$(document).ready(function(){
    init();
});

function init() {
    initDatePickers();
}

function initDatePickers() {
    var dateFrom = $('#date-from'),
        dateTo = $('#date-to');

    dateFrom.change(function(){
       makeRequests(); 
    });
    
    dateTo.change(function(){
       makeRequests(); 
    });
}

function makeRequests() {    
    var dateFromField = $('#date-from'),
        dateToField = $('#date-to'),
        dateFrom = dateFromField.val(),
        dateTo = dateToField.val();  

        
    if (!dateFrom || !dateTo) {
        return;
    }
    
    makeRequest('one', dateFrom, dateTo);
    makeRequest('two', dateFrom, dateTo);
    makeRequest('three', dateFrom, dateTo);
}

function makeRequest(section, dateFrom, dateTo) {
    var url = 'https://app.alegra.com/report/retrieve-profit-and-loss/section/' + section + '/format/json?&from=' + dateFrom + '&to=' + dateTo + '&node=root';
    
    $.ajax({
        url: url,
        success: function(response, text, jqXhr) {
            if (response && response.success) {
                var table = $('#section-' + section);
                
                renderTable(response, table, '');
            } else {
                showError('Error retrieving section one');
            }
        }
    });
}

function renderTable(root, table, blanks) {
    blanks = blanks || '';
    
    var text = root.text || 'Total',
        htmlArray = [
        '<tr>',
            '<td>',
                blanks + text,
            '</td>',
            '<td>',
                root.total,
            '</td>',
        '</tr>'
    ],
    tr = $(htmlArray.join(''));
    
    table.append(tr);
    
    $.each(root.children, function(index, child){
        renderTable(child, table, blanks + '&nbsp;&nbsp;');
    });
}