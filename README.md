Profit and Loss Export

Small program to view the Alegra's Profit and Loss report as a table that can be copied to Excel.


Making it work with chrome (for Mac):

Close Chrome and then run the follwoing command from terminal:

open -a Google\ Chrome --args --disable-web-security

Go to Alegra and login.

Open the index.html on Chrome.

Copy the profit and loss.

Paste in Excel.

Close Chrome (Very important)
